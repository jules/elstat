require "kemal"
require "logger"
require "time"
require "big"

require "../helpers"
require "../adapters"

def get_status(manager)
  res = {} of String => Hash(String, String | Bool | Int64 | UInt64)

  manager.service_names.each do |serv_name|
    state = manager.@state[serv_name]
    thresholds = manager.@context.thresholds serv_name
    res[serv_name] = state.merge({
      "description"       => manager.@context.cfg["service:#{serv_name}"]["description"],
      "down_threshold"    => thresholds[:down],
      "slow_threshold"    => thresholds[:slow],
      "latency_threshold" => thresholds[:latency],
    })
  end

  return res
end

alias GraphElement = Tuple(Int64, Int64)

def get_graphs(manager) : Hash(String, Array(GraphElement))
  res = {} of String => Array(GraphElement)

  manager.service_names.each do |serv_name|
    cols = manager.column_names(serv_name)
    if !cols.includes?("latency")
      next
    end

    db = manager.@context.db

    rows = db.query_all(
      "select timestamp, latency from #{serv_name}
      order by timestamp desc
      limit 50", as: {Int64, Int64})

    res[serv_name] = Array(GraphElement).new

    rows.each do |row|
      res[serv_name].push(row)
    end
  end

  return res
end

alias UptimeData = String

def raw_uptime(manager, name : String, period) : UptimeData
  max_tstamp = (Time.utc - Time::UNIX_EPOCH).total_milliseconds
  min_tstamp = ((Time.utc - period) - Time::UNIX_EPOCH).total_milliseconds

  period_ms = max_tstamp - min_tstamp

  db = manager.@context.db
  cfg = manager.@context.cfg

  down_hits = db.scalar(
    "select count(*) from #{name}
    where status = 0 and timestamp > ?",
    min_tstamp).as(Int64)

  # calculate downtime in milliseconds and then convert it to a
  # percentage from the total period_ms
  downtime = down_hits * cfg["service:#{name}"]["poll"].to_u64 * 1000
  downtime = BigFloat.new(downtime)
  downtime = downtime / BigFloat.new(period_ms)

  one_hundred = BigFloat.new(100)
  uptime = one_hundred - downtime
  return uptime.to_s
end

def get_uptime(manager, period : (Time::Span | Time::MonthSpan))
  res = {} of String => UptimeData
  manager.service_keys.each do |serv_key|
    name = serv_key.lchop("service:")
    res[name] = raw_uptime(manager, name, period)
  end
  return res
end

def quick_response(manager)
  return {
    status:       get_status(manager),
    uptime:       get_uptime(manager, 1.day),
    week_uptime:  get_uptime(manager, 1.week),
    month_uptime: get_uptime(manager, 1.month),
  }
end

get "/api/quick" do |env|
  manager = get_manager env
  quick_response(manager).to_json
end

get "/api/status" do |env|
  manager = get_manager env
  quick_response(manager).merge({graph: get_graphs(manager)}).to_json
end
