require "kemal"
require "logger"
require "./manager"

add_context_storage_type(Logger)
add_context_storage_type(Manager)

def get_logger(env)
  log_opt = env.get? "log"
  if !log_opt.is_a?(Logger)
    raise "no logger"
  else
    log_opt
  end
end

def get_manager(env)
  opt = env.get? "manager"
  if !opt.is_a?(Manager)
    raise "no manager"
  else
    opt
  end
end
