require "../context"
require "../adapters"
require "../api/incidents"

require "time"

# Main superclass for alerts.
abstract class Alert
  def initialize(@alert_name : String, @ctx : Context)
  end

  abstract def alert_err(serv_name : String, result : AdapterResult)
  abstract def alert_err_ok(serv_name : String, result : AdapterResult)
  abstract def alert_slow(serv_name : String, result : AdapterResult)
  abstract def alert_slow_ok(serv_name : String, result : AdapterResult)
  abstract def alert_incident_new(incident : Incident)
  abstract def alert_incident_close(incident : Incident)
  abstract def alert_incident_new_stage(incident : Incident)

  def calc_downtime(service_name : String, result) : Time::Span
    poll_period = @ctx.cfg["service:#{service_name}"]["poll"].to_i64
    thresholds = @ctx.thresholds(service_name)

    # find the last success for service before the downtime period
    # it is important we do a timestamp clause inside 'WHERE' because
    # there can be more than one success as max(timestamp)
    @ctx.log.debug("result timestamp = #{result["timestamp"]}")
    @ctx.log.debug("down thres = #{thresholds[:down]}, poll #{poll_period}")

    up_window =
      ((thresholds[:down] * (poll_period + 1)) * 1000)

    @ctx.log.debug("up window of #{up_window}ms")

    ts_success = @ctx.db.scalar(
      "SELECT max(timestamp)
       FROM #{service_name}
       WHERE status = 1 AND timestamp < ?
       LIMIT 1", result["timestamp"].as(Int64) - up_window)

    # count all the downtime that happened since then
    # we do this by counting all the results that failed, then
    # multiplying that by the currently set poll time on the service
    down_hits = @ctx.db.scalar(
      "SELECT COUNT(*)
       FROM #{service_name}
       WHERE timestamp > ?
         AND status = 0", ts_success).as(Int64)

    @ctx.log.debug("ts_success = #{ts_success}")
    @ctx.log.warn("calculated downtime hits: #{down_hits}")

    Time::Span.new(
      seconds: down_hits * poll_period, nanoseconds: 0
    )
  end
end
