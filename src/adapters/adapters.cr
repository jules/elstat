alias AdapterValue = (String | Int64 | Bool)
alias AdapterResult = Hash(String, AdapterValue)

class AdapterError < Exception
end
