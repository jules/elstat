require "http"
require "./adapter"
require "../status_codes"

class HTTPAdapter < Adapter
  def self.spec
    {db: ["timestamp", "status", "latency"]}
  end

  # Get a human-friendly phrase describing the error code.
  def self.get_phrase(status_code : String) : String
    text = STATUS[status_code]?
    return text || "Unknown status"
  end

  def self.query(ctx, serv_cfg)
    start = Time.monotonic
    begin
      resp = HTTP::Client.get(serv_cfg["http_url"])
    rescue ex
      raise AdapterError.new("http error: #{ex.message}")
    end
    elapsed = (Time.monotonic - start).total_milliseconds.to_i64
    status = resp.status_code

    success = status == 200 || status == 204
    # success = false

    # raise "piss"

    if !success
      elapsed = 0.to_f64
      status_phrase = self.get_phrase(status.to_s)
      raise AdapterError.new("http status #{status} - #{status_phrase}")
    end

    return self.construct [success, elapsed]
  end
end
