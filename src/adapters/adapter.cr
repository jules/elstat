require "./adapters"

class Adapter
  def self.spec
    raise "adapter has no spec"
  end

  def self.query(ctx : Context, serv_cfg : Service) : AdapterResult
    raise "not implemented"
  end

  private def self.construct(res_array : Array(AdapterValue)) : AdapterResult
    spec = self.spec
    cols = spec[:db][1, spec.size + 1]
    res = AdapterResult.new

    cols.map_with_index do |column, idx|
      res[column] = res_array[idx]
    end

    return res
  end
end
