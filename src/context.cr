require "ini"
require "logger"
require "db"
require "sqlite3"

class Context
  property cfg = INI.parse(File.read("config.ini"))
  property log = Logger.new(STDOUT)
  property db : DB::Database = DB.open("sqlite3://elstat.db")

  @FRONTEND_BUILD_FOLDER = "./priv/frontend/build"

  def setup
    root = cfg["elstat"]
    if root["debug"]? == "1"
      log.info("setting logger to debug")
      log.level = Logger::DEBUG
    end

    if root["frontend_disable"]? != "1"
      begin
        Dir.open @FRONTEND_BUILD_FOLDER
      rescue ex : IO::Error
        log.error("frontend is required by default, frontend was not found. stopping. #{ex}")
      end
    end
  end

  def thresholds(serv_name : String) : NamedTuple(slow: UInt64, down: UInt64, latency: UInt64)
    serv = @cfg["service:#{serv_name}"]
    global = @cfg["elstat"]

    slow_threshold = (serv["slow_threshold"]? || global["slow_threshold"]).to_u64
    down_threshold = (serv["down_threshold"]? || global["down_threshold"]).to_u64
    latency_threshold = (serv["slow_threshold_ms"]? || global["slow_threshold_ms"]).to_u64

    {slow: slow_threshold, down: down_threshold, latency: latency_threshold}
  end
end
