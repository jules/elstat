# Elstat

Status page software.

## Installing

requirements:
 - crystal 0.29.0

```
git clone https://gitlab.com/elixire/elstat
cd elstat

# edit config.ini as you wish
cp config.example.ini config.ini

shards install
shards build --production

# build frontend
# check instructions on priv/frontend/README.md
cd priv/frontend
```

## Making a release tarfile

This is relatively selfcontained:
 - Statically linked elstat on release mode
 - Runs frontend build

Since the frontend uses `config.json` as a template
for it's data, frontend builds are NOT self-contained, if
you wish to have a different frontend config, you must
re-do a release. This is also why there aren't any official
tarfile releases of elstat.

```
make
```

## Run

As of right now, it is required to run elstat on the same directory as the
folder containing the source.

```
./bin/elstat
```

## Run tests

```
crystal spec
```
