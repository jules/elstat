all: release

clean:
	rm -f release.tar release.tar.gz

release:
	shards build --progress --release --static
	cd priv/frontend; npm run build
	tar cvf ./release.tar ./bin/elstat ./priv/frontend/build ./config.example.ini
	xz ./release.tar
